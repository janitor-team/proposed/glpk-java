glpk-java (1.12.0-2) unstable; urgency=medium

  * Bump to debhelper 13
  * debian/rules: simplify using debhelper 12.8’s execute_after_* rules
  * disable-relax4.patch: new patch, fixes FTBFS (Closes: #978311)
  * debian/watch: bump to format v4
  * debian/control: add Rules-Requires-Root: no
  * Bump to S-V 4.5.1

 -- Sébastien Villemot <sebastien@debian.org>  Thu, 14 Jan 2021 15:06:27 +0100

glpk-java (1.12.0-1) unstable; urgency=medium

  * New upstream version 1.12.0
  * Bump to debhelper compat level 11.
  * Update Vcs-* URLs for move to salsa.
  * Bump Standards-Version to 4.1.3.

 -- Sébastien Villemot <sebastien@debian.org>  Thu, 08 Mar 2018 10:35:41 +0100

glpk-java (1.11.0-1) unstable; urgency=medium

  * New upstream version 1.11.0
  * Bump Standards-Version to 4.1.2.

 -- Sébastien Villemot <sebastien@debian.org>  Sun, 10 Dec 2017 11:32:55 +0100

glpk-java (1.10.0-1) unstable; urgency=medium

  * New upstream version 1.10.0
  * d/control: bump Standards-Version to 4.0.1.

 -- Sébastien Villemot <sebastien@debian.org>  Sun, 06 Aug 2017 23:55:50 +0200

glpk-java (1.9.0-1) unstable; urgency=medium

  * New upstream version 1.9.0
  * d/control:
    + Bump Standards-Version to 4.0.0.
    + Tighten B-D on default-jdk to require at least 1.8.
    + Mark libglpk-java as M-A same.

 -- Sébastien Villemot <sebastien@debian.org>  Tue, 04 Jul 2017 16:34:40 +0200

glpk-java (1.8.0-1) unstable; urgency=medium

  * New upstream version 1.8.0.
  * Bump to debhelper compat level 10.
  * d/copyright:
    + Reflect upstream changes.
    + Use secure URL for format.
  * d/control: use canonical URL for Vcs-Browser.
  * d/rules: pass --disable-maven to configure. Otherwise, if maven is
    installed, the build system starts downloading tons of Java packages.

 -- Sébastien Villemot <sebastien@debian.org>  Fri, 09 Jun 2017 11:24:32 +0200

glpk-java (1.7.0-1) unstable; urgency=medium

  * Imported Upstream version 1.7.0
  * d/copyright: reflect upstream changes.
  * d/control:
    + Bump Standards-Version to 3.9.8, no changes needed.
    + Use secure URLs for Vcs-* fields.

 -- Sébastien Villemot <sebastien@debian.org>  Tue, 17 May 2016 15:54:10 +0200

glpk-java (1.3.1-1) unstable; urgency=medium

  * Imported Upstream version 1.3.1

 -- Sébastien Villemot <sebastien@debian.org>  Wed, 20 Jan 2016 14:02:23 +0100

glpk-java (1.3.0-1) unstable; urgency=medium

  * Imported Upstream version 1.3.0

 -- Sébastien Villemot <sebastien@debian.org>  Sat, 14 Nov 2015 15:38:52 +0100

glpk-java (1.2.0-1) unstable; urgency=medium

  * Imported Upstream version 1.2.0
  * d/copyright: reflect upstream changes.

 -- Sébastien Villemot <sebastien@debian.org>  Sun, 04 Oct 2015 21:22:51 +0200

glpk-java (1.1.0-1) unstable; urgency=medium

  * Imported Upstream version 1.1.0
  * Bump Standards-Version to 3.9.6, no changes needed.

 -- Sébastien Villemot <sebastien@debian.org>  Sun, 06 Sep 2015 19:17:20 +0200

glpk-java (1.0.37-1) unstable; urgency=medium

  * Imported Upstream version 1.0.37

 -- Sébastien Villemot <sebastien@debian.org>  Sat, 30 Aug 2014 10:03:53 +0200

glpk-java (1.0.36-1) unstable; urgency=medium

  * Imported Upstream version 1.0.36

 -- Sébastien Villemot <sebastien@debian.org>  Thu, 12 Jun 2014 10:45:35 +0200

glpk-java (1.0.34-1) unstable; urgency=medium

  * Imported Upstream version 1.0.34
    - fixes memory access on big endian systems. (Closes: #745214)

 -- Sébastien Villemot <sebastien@debian.org>  Wed, 23 Apr 2014 15:43:31 +0200

glpk-java (1.0.33-1) unstable; urgency=medium

  * Imported Upstream version 1.0.33
  * Tighten the B-D on default-jdk, so that only archs with OpenJDK satisfy it.
    (Closes: #743131)

 -- Sébastien Villemot <sebastien@debian.org>  Thu, 03 Apr 2014 17:13:31 +0200

glpk-java (1.0.32-1) unstable; urgency=medium

  * Imported Upstream version 1.0.32

 -- Sébastien Villemot <sebastien@debian.org>  Wed, 19 Feb 2014 18:52:04 +0100

glpk-java (1.0.31-2) unstable; urgency=medium

  * Upload to unstable for glpk transition.
  * Bump Standards-Version to 3.9.5, no changes needed.

 -- Sébastien Villemot <sebastien@debian.org>  Thu, 05 Dec 2013 18:45:44 +0100

glpk-java (1.0.31-1) experimental; urgency=low

  * Imported Upstream version 1.0.31
    - this release is compatible with GLPK 4.52.1 (Closes: #714361)
  * debian/copyright: reflect upstream changes
  * debian/watch: reflect change of upstream tarball name
  * debian/control:
    + use canonical URLs for Vcs-* fields
    + add myself to uploaders
    + upgrade to Standards-Version 3.9.4, no changes needed
  * debian/rules: rewrite using dh
  * Add lintian overrides for codeless JARs
  * Register PDF documentation into doc-base

 -- Sébastien Villemot <sebastien@debian.org>  Wed, 07 Aug 2013 21:28:14 +0200

glpk-java (1.0.18-1) unstable; urgency=low

  * Team upload
  * New upstream release
  * Package moved to the Debian Science team
  * Standards-Version updated to version 3.9.2
  * Cleanup uploader list (Closes: #571829)
  * Update to the default-jdk & default-jre packages (Closes: #599117)
  * glpk-java.pdf is now installed (Closes: #599119)
  * Provide the path to jni.h with the JAVA_HOME variable (Closes: #618167)
  * Switch to dpkg-source 3.0 (quilt) format
  * Do not compress pdf
  * Do not ship .la file

 -- Sylvestre Ledru <sylvestre@debian.org>  Thu, 04 Aug 2011 00:09:49 +0200

glpk-java (1.0.13-4) unstable; urgency=medium

  [ Kumar Appaiah ]
  * Use update patch from Heinrich Schuchardt which
    reworks the callback interface between libglpk-java
    and GLPK, and updates the Makefiles.

 -- Debian Scientific Computation Team <pkg-scicomp-devel@lists.alioth.debian.org>  Fri, 10 Sep 2010 21:15:56 +0000

glpk-java (1.0.13-3) unstable; urgency=medium

  [ Kumar Appaiah ]
  * Use patch from Xypron to:
    - change sourcepath to classpath in swig/Makefile, as
      the former is ignored by gcj.
    - Remove extraneous overrides in examples.
    (Closes: #576896)
  * Standards Version is now 3.9.1

 -- Debian Scientific Computation Team <pkg-scicomp-devel@lists.alioth.debian.org>  Wed, 08 Sep 2010 17:11:30 -0500

glpk-java (1.0.13-2) unstable; urgency=low

  * Update minimum dependency of libglpk-dev to 4.43
    (due to glpk_error_hook()) as pointed out by
    Heinrich Schuchardt.

 -- Debian Scientific Computation Team <pkg-scicomp-devel@lists.alioth.debian.org>  Sun, 21 Mar 2010 15:07:49 -0500

glpk-java (1.0.13-1) unstable; urgency=low

  [ Kumar Appaiah ]
  * New Upstream Release.
    + Fix FTBFS on amd64. (Closes: #572958)
  * debian/control:
    + Standards Version is now 3.8.4 (No changes needed).
  * debian/rules:
    + Rename "test" target to "check".

 -- Debian Scientific Computation Team <pkg-scicomp-devel@lists.alioth.debian.org>  Thu, 18 Mar 2010 17:58:16 -0500

glpk-java (1.0.1-1.1) unstable; urgency=low

  * Non-maintainer upload.
  * Add correct build-deps, closes: #536611
    Thanks once a again to Peter Green <plugwash@p10link.net> for fix

 -- Riku Voipio <riku.voipio@iki.fi>  Thu, 05 Nov 2009 23:39:38 +0200

glpk-java (1.0.1-1) unstable; urgency=low

  * Initial release (closes: #532185)

 -- Rafael Laboissiere <rafael@debian.org>  Sat, 06 Jun 2009 11:53:55 +0200
